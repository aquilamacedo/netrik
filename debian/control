Source: netrik
Section: web
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               libncurses-dev
Standards-Version: 4.6.2
Homepage: http://netrik.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/netrik.git
Vcs-Browser: https://salsa.debian.org/debian/netrik

Package: netrik
Architecture: any
Provides: www-browser
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: text mode WWW browser with vi like keybindings
 Netrik is an advanced text mode WWW browser. Its purpose is to give access to
 as much of the Web as possible in text mode, without forsaking any comfort.
 The user interface is looking roughly like a combination of gVim and PINE.
 .
 Some of the core ideas (not all implemented yet):
 .
  * Context-(URL-)sensitive setup
  * Partially loaded pages
  * Half-graphical mode
  * Efficient navigation
  * Key mapping and macros (similar to vi and mutt)
  * Command prompt, menus and online help
 .
 netrik does neither support HTTPS nor IPv6 yet.
